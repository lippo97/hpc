/****************************************************************************
 *
 * omp-earthquake.c - Simple 2D earthquake model
 *
 * Filippo Nardini
 * Matricola: 0000793827
 *
 * Per eseguire il programma si puo' usare la riga di comando seguente:
 *
 * ./omp-earthquake 100000 256 > out
 *
 * Il primo parametro indica il numero di timestep, e il secondo la
 * dimensione (lato) del dominio. L'output consiste in coppie di
 * valori numerici (100000 in questo caso) il cui significato e'
 * spiegato nella specifica del progetto.
 *
 ****************************************************************************/
#include "hpc.h"
#include <stdio.h>
#include <stdlib.h>     /* rand() */
#include <assert.h>

/* energia massima */
#define EMAX 4.0f
/* energia da aggiungere ad ogni timestep */
#define EDELTA 1e-4


/**
 * Restituisce un puntatore all'elemento di coordinate (i,j) del
 * dominio grid con n colonne.
 */
static inline float *IDX(float *grid, int i, int j, int n)
{
    return (grid + i*n + j);
}

/**
 * Restituisce un numero reale pseudocasuale con probabilita' uniforme
 * nell'intervallo [a, b], con a < b.
 */
float randab( float a, float b )
{
    return a + (b-a)*(rand() / (float)RAND_MAX);
}

/**
 * Inizializza il dominio grid di dimensioni n*n con valori di energia
 * scelti con probabilità uniforme nell'intervallo [fmin, fmax], con
 * fmin < fmax.
 *
 * NON PARALLELIZZARE QUESTA FUNZIONE: rand() non e' thread-safe,
 * qundi non va usata in blocchi paralleli OpenMP; inoltre la funzione
 * non si "comporta bene" con MPI (i dettagli non sono importanti, ma
 * posso spiegarli a chi e' interessato). Di conseguenza, questa
 * funzione va eseguita dalla CPU, e solo dal master (se si usa MPI).
 */
void setup( float* grid, int n, float fmin, float fmax )
{
    for ( int i=0; i<n; i++ ) {
        for ( int j=0; j<n; j++ ) {
            if (i == 0 || j == 0 || j == (n-1) || i == (n-1)) {
                *IDX(grid, i, j, n) = 0;
            } else {
                *IDX(grid, i, j, n) = randab(fmin, fmax);
            }
        }
    }
}

/**
 * Somma delta a tutte le celle del dominio grid di dimensioni
 * n*n. Questa funzione realizza il passo 1 descritto nella specifica
 * del progetto.
 */
void increment_energy( float *grid, int n, float delta )
{
#pragma omp parallel for 
    for (int i=1; i<n-1; i++) {
        for (int j=1; j<n-1; j++) {
            *IDX(grid, i, j, n) += delta;
        }
    }
}

/**
 * Restituisce il numero di celle la cui energia e' strettamente
 * maggiore di EMAX.
 */
int count_cells( float *grid, int n )
{
    int c = 0;
#pragma omp parallel for reduction(+:c)
    for (int i=1; i<n-1; i++) {
        for (int j=1; j<n-1; j++) {
            if ( *IDX(grid, i, j, n) > EMAX ) { c++; }
        }
    }
    return c;
}

/** 
 * Distribuisce l'energia di ogni cella a quelle adiacenti (se
 * presenti). cur denota il dominio corrente, next denota il dominio
 * che conterra' il nuovo valore delle energie. Questa funzione
 * realizza il passo 2 descritto nella specifica del progetto.
 */
void propagate_energy( float *cur, float *next, int n )
{
    const float FDELTA = EMAX/4;
#pragma omp parallel for
    for (int i=1; i<n-1; i++) {
        for (int j=1; j<n-1; j++) {
            float F = *IDX(cur, i, j, n);
            float *out = IDX(next, i, j, n);

            /* Se l'energia del vicino di sinistra (se esiste) e'
               maggiore di EMAX, allora la cella (i,j) ricevera'
               energia addizionale FDELTA = EMAX/4 */
            if ((*IDX(cur, i, j-1, n) > EMAX)) { F += FDELTA; }
            if ((*IDX(cur, i, j+1, n) > EMAX)) { F += FDELTA; }
            if ((*IDX(cur, i-1, j, n) > EMAX)) { F += FDELTA; }
            if ((*IDX(cur, i+1, j, n) > EMAX)) { F += FDELTA; }

            if (F > EMAX) {
                F -= EMAX;
            }

            /* Si noti che il valore di F potrebbe essere ancora
               maggiore di EMAX; questo non e' un problema:
               l'eventuale eccesso verra' rilasciato al termine delle
               successive iterazioni vino a riportare il valore
               dell'energia sotto la foglia EMAX. */
            *out = F;
        }
    }
}

/**
 * Restituisce l'energia media delle celle del dominio grid di
 * dimensioni n*n. Il dominio non viene modificato.
 */
float average_energy(float *grid, int n)
{
    float sum = 0.0f;
#pragma omp parallel for reduction(+:sum)
    for (int i=1; i<n-1; i++) {
        for (int j=1; j<n-1; j++) {
            sum += *IDX(grid, i, j, n);
        }
    }
    return (sum / ((n-2)*(n-2)));
}

int main( int argc, char* argv[] )
{
    float *cur, *next;
    int s, n = 256, nsteps = 2048;
    float Emean;
    int c;

    srand(19); /* Inizializzazione del generatore pseudocasuale */
    
    if ( argc > 3 ) {
        fprintf(stderr, "Usage: %s [nsteps [n]]\n", argv[0]);
        return EXIT_FAILURE;
    }

    if ( argc > 1 ) {
        nsteps = atoi(argv[1]);
    }

    if ( argc > 2 ) {
        n = atoi(argv[2]);
    }

    /*const size_t size = n*n*sizeof(float);*/
    const size_t ghost_size = (n+2)*(n+2)*sizeof(float);

    /* Allochiamo i domini */
    cur = (float*)malloc(ghost_size); assert(cur);
    next = (float*)malloc(ghost_size); assert(next);

    /* L'energia iniziale di ciascuna cella e' scelta 
       con probabilita' uniforme nell'intervallo [0, EMAX*0.1] */       
    setup(cur, n+2, 0, EMAX*0.1);
    
    const double tstart = hpc_gettime();
    for (s=0; s<nsteps; s++) {
        /* L'ordine delle istruzioni che seguono e' importante */
        increment_energy(cur, n+2, EDELTA);
        c = count_cells(cur, n+2);
        propagate_energy(cur, next, n+2);
        Emean = average_energy(next, n+2);

        printf("%d %f\n", c, Emean);

        float *tmp = cur;
        cur = next;
        next = tmp;
    }
    const double elapsed = hpc_gettime() - tstart;
    
    double Mupdates = (((double)n)*n/1.0e6)*nsteps; // milioni di celle aggiornate per ogni secondo di wall clock time
    fprintf(stderr, "%s : %.4f Mupdates in %.4f seconds (%f Mupd/sec)\n", argv[0], Mupdates, elapsed, Mupdates/elapsed);

    /* Libera la memoria */
    free(cur);
    free(next);
    
    return EXIT_SUCCESS;
}
