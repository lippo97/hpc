/****************************************************************************
 *
 * mpi-earthquake.c - Simple 2D earthquake model 
 *
 * Filippo Nardini
 * Matricola: 0000793827
 *
 * Per eseguire il programma si puo' usare la riga di comando seguente:
 *
 * ./mpi-earthquake 100000 256 > out
 *
 * Il primo parametro indica il numero di timestep, e il secondo la
 * dimensione (lato) del dominio. L'output consiste in coppie di
 * valori numerici (100000 in questo caso) il cui significato e'
 * spiegato nella specifica del progetto.
 *
 ****************************************************************************/
#include "hpc.h"
#include <mpi.h> 
#include <stdio.h>
#include <stdlib.h>     /* rand() */
#include <assert.h>
#include <string.h>     /* memset() */

/* energia massima */
#define EMAX 4.0f
/* energia da aggiungere ad ogni timestep */
#define EDELTA 1e-4

/**
 * Restituisce un puntatore all'elemento di coordinate (i,j) del
 * dominio grid con n colonne.
 */
static inline float *IDX(float *grid, int i, int j, int n)
{
    return (grid + i*n + j);
}

/**
 * Restituisce un numero reale pseudocasuale con probabilita' uniforme
 * nell'intervallo [a, b], con a < b.
 */
float randab( float a, float b )
{
    return a + (b-a)*(rand() / (float)RAND_MAX);
}

/**
 * Inizializza il dominio grid di dimensioni n*n con valori di energia
 * scelti con probabilità uniforme nell'intervallo [fmin, fmax], con
 * fmin < fmax.
 *
 * NON PARALLELIZZARE QUESTA FUNZIONE: rand() non e' thread-safe,
 * qundi non va usata in blocchi paralleli OpenMP; inoltre la funzione
 * non si "comporta bene" con MPI (i dettagli non sono importanti, ma
 * posso spiegarli a chi e' interessato). Di conseguenza, questa
 * funzione va eseguita dalla CPU, e solo dal master (se si usa MPI).
 */
void setup( float* grid, int n, float fmin, float fmax )
{
    for ( int i=0; i<n; i++ ) {
        for ( int j=0; j<n; j++ ) {
            *IDX(grid, i, j, n) = randab(fmin, fmax);
        }
    }
}

/**
 * Somma delta a tutte le celle del dominio grid di dimensioni
 * n*n. Questa funzione realizza il passo 1 descritto nella specifica
 * del progetto.
 */
void increment_energy( float *grid, int n, float delta )
{
    for (int i = 0; i < n; ++i) {
        grid[i] += delta;
    }
}

/**
 * Restituisce il numero di celle la cui energia e' strettamente
 * maggiore di EMAX.
 */
int count_cells( float *grid, int n )
{
    int c = 0;
    for (int i = 0; i < n; ++i) {
        if (grid[i] > EMAX) { 
            c++;
        }
    }
    return c;
}

/** 
 * Distribuisce l'energia di ogni cella a quelle adiacenti (se
 * presenti). cur denota il dominio corrente, next denota il dominio
 * che conterra' il nuovo valore delle energie. Questa funzione
 * realizza il passo 2 descritto nella specifica del progetto.
 */
void propagate_energy( float *cur, float *next, int rows, int cols )
{
    const float FDELTA = EMAX/4;
    for (int i=0; i<rows; i++) {
        for (int j=0; j<cols; j++) {
            float F = *IDX(cur, i, j, cols);
            float *out = IDX(next, i, j, cols);

            if ((j>0) && (*IDX(cur, i, j-1, cols) > EMAX)) { F += FDELTA; }
            if ((j<cols-1) && (*IDX(cur, i, j+1, cols) > EMAX)) { F += FDELTA; }
            if ((*IDX(cur, i-1, j, cols) > EMAX)) { F += FDELTA; }
            if ((*IDX(cur, i+1, j, cols) > EMAX)) { F += FDELTA; }

            if (F > EMAX) {
                F -= EMAX;
            }

            *out = F;
        }
    }
}

/**
 * Restituisce la somma dei valori di tutte le celle del dominio.
 */
float sum_energy(float *grid, int n)
{
    float sum = 0.0f;
    for (int i = 0; i < n; ++i) {
        sum += grid[i];
    }
    return sum;
}

int main( int argc, char* argv[] )
{
    float *cur;
    int s, n = 256, nsteps = 2048;
    float local_sum, sum;
    int local_c, c;
    int my_rank, comm_sz;

    srand(19); /* Inizializzazione del generatore pseudocasuale */

    if ( argc > 3 ) {
        fprintf(stderr, "Usage: %s [nsteps [n]]\n", argv[0]);
        return EXIT_FAILURE;
    }

    if ( argc > 1 ) {
        nsteps = atoi(argv[1]);
    }

    if ( argc > 2 ) {
        n = atoi(argv[2]);
    }

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &comm_sz);

    /**
     * Solo il primo processo crea la matrice iniziale
     */
    if (my_rank == 0) {
      const size_t size = n*n*sizeof(float);
      cur = (float*)malloc(size); assert(cur);
      setup(cur, n, 0, EMAX*0.1);
    }

    /**
     * Calcolo la suddivisione della matrice iniziale 
     * tra i vari processi
     */
    const int rows_each = n / comm_sz;
    const int leftovers = n % comm_sz;
    int *sendcounts = (int*)malloc(comm_sz * sizeof(*sendcounts));
    int *displs = (int*)malloc(comm_sz * sizeof(*displs));

    int offset = 0;
    for (int i = 0; i < comm_sz; ++i) {
        const int my_rows = rows_each + (i < leftovers); 
        const int my_size = my_rows * n;
        sendcounts[i] = my_size;
        displs[i] = offset;
        offset += my_size;
    }

    /**
     * Ciascun processo riceve il proprio numero di celle e
     * calcola il proprio numero di righe.
     * Dopodiche' alloca le due matrici locali cur e next.
     */
    const int local_n = sendcounts[my_rank];
    const int local_rows = local_n / n;
    size_t halo = n;
    size_t grid_size = (local_n + 2*halo) * sizeof(float);

    float *local_cur  = (float*)malloc(grid_size);
    float *local_next = (float*)malloc(grid_size);

    const int rank_prev = (my_rank - 1 + comm_sz) % comm_sz;
    const int rank_next = (my_rank + 1) % comm_sz;

    memset(local_cur, 0, grid_size);

    MPI_Scatterv( cur,           /* sendbuf             */
                  sendcounts,    /* sendcounts          */
                  displs,        /* displacements       */
                  MPI_FLOAT,    /* sent datatype       */
                  local_cur + halo,       /* recvbuf             */
                  local_n,       /* recvcount           */
                  MPI_FLOAT,    /* received datatype   */
                  0,             /* source              */
                  MPI_COMM_WORLD /* communicator        */
                  );
    free(sendcounts);
    free(displs);

    const double tstart = hpc_gettime();
    for (s=0; s<nsteps; s++) {
        increment_energy(local_cur + halo, local_n, EDELTA);

        /**
         * Ciscun processo effettua il conteggio nel proprio dominio per poi effettuare una reduce
         */
        local_c = count_cells(local_cur + halo, local_n);
        MPI_Reduce(
             &local_c,
             &c,
             1,
             MPI_INT,
             MPI_SUM,
             0,
             MPI_COMM_WORLD
        );

        // Scambio delle ghost dall'alto al basso
        if (my_rank == 0) { // Primo processo
            MPI_Send(
                local_cur + halo + local_n - n,
                n,
                MPI_FLOAT,
                rank_next,
                0,
                MPI_COMM_WORLD
            );
        } else if(my_rank == comm_sz-1) { // Ultimo processo
            MPI_Recv(
                local_cur,
                n,
                MPI_FLOAT,
                rank_prev,
                0,
                MPI_COMM_WORLD,
                MPI_STATUS_IGNORE
            );
        } else { // Processo generico
            MPI_Sendrecv(
                local_cur + halo + local_n - n,
                n,
                MPI_FLOAT,
                rank_next,
                0,
                local_cur,
                n,
                MPI_FLOAT,
                rank_prev,
                0,
                MPI_COMM_WORLD,
                MPI_STATUS_IGNORE
            );
        }

        // Scambio delle ghost dal basso verso l'alto
        if (my_rank == 0) { // Primo processo
            MPI_Recv(
                local_cur + halo + local_n,
                n,
                MPI_FLOAT,
                rank_next,
                0,
                MPI_COMM_WORLD,
                MPI_STATUS_IGNORE
            );
        } else if(my_rank == comm_sz-1) { // Ultimo processo 
            MPI_Send(
                local_cur + halo,
                n,
                MPI_FLOAT,
                rank_prev,
                0,
                MPI_COMM_WORLD
            );
        } else { // Processo generico
            MPI_Sendrecv(
                local_cur + halo,
                n,
                MPI_FLOAT,
                rank_prev,
                0,
                local_cur + halo + local_n,
                n,
                MPI_FLOAT,
                rank_next,
                0,
                MPI_COMM_WORLD,
                MPI_STATUS_IGNORE
            );
        }

        const int rows = local_rows, cols = n;
        propagate_energy(local_cur + halo, local_next + halo, rows, cols);

        /**
         * Per calcolare la media si calcola prima la somma di tutte le celle
         * per poi dividerla per il numero di queste.
         */
        local_sum = sum_energy(local_next + halo, local_n);
        MPI_Reduce(
            &local_sum,
            &sum,
            1,
            MPI_FLOAT,
            MPI_SUM,
            0,
            MPI_COMM_WORLD
        );

        /**
         * Solo un processo poi si occupa di calcolare l'energia media
         * e di stamparla sullo stdout
         */
        if (my_rank == 0) {
            float Emean = sum / (n*n);
            printf("%d %f\n", c, Emean);
        }

        float *tmp = local_cur;
        local_cur = local_next;
        local_next = tmp;
    }
    const double elapsed = hpc_gettime() - tstart;
    
    double Mupdates = (((double)n)*n/1.0e6)*nsteps;
    MPI_Barrier(MPI_COMM_WORLD);
    if (my_rank == 0) {
      fprintf(stderr, "%s[%d] : %.4f Mupdates in %.4f seconds (%f Mupd/sec)\n", argv[0], my_rank, Mupdates, elapsed, Mupdates/elapsed);
    }

    /* Libera la memoria */
    free(local_cur);
    free(local_next);

    if (my_rank == 0) {
#pragma GCC diagnostic ignored "-Wmaybe-uninitialized"
        free(cur);
    }

    MPI_Finalize();
    
    return EXIT_SUCCESS;
}
